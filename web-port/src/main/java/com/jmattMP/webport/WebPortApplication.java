package com.jmattMP.webport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.jmattMP.webport.configuration")
public class WebPortApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebPortApplication.class, args);
	}

}
