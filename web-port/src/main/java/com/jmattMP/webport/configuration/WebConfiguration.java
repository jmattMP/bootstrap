package com.jmattMP.webport.configuration;

import com.jmattMP.webport.HealthEndpoint;
import com.jmattMP.webport.HealthPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfiguration {

    @Bean
    public HealthPoint healthPoint(){
        return new HealthPoint();
    }

    @Bean
    public HealthEndpoint healthEndpoint2() {
        return new HealthEndpoint();
    }
}
