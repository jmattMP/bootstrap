# Getting Started

### Actuator endpoints
For further reference, please consider the following sections:

*/auditevents — lists security audit-related events such as user login/logout. Also, we can use this to filter by principal or type among other fields.
* /beans — returns all available beans in our BeanFactory. Unlike /auditevents, it doesn't support filtering.
*  /conditions — formerly known as  /autoconfig, this endpoint builds a report of conditions around auto-configuration.
* /configprops — allows us to fetch all @ConfigurationProperties beans.
* /env — returns the current environment properties. Additionally, we can retrieve single properties.
* /flyway — provides details about our Flyway database migrations.
* /health – summarizes the health status of our application.
* /heapdump — builds and returns a heap dump from the JVM used by our application.
* /info — returns general information. It might be custom data, build information, or details about the latest commit.
* /liquibase — behaves like /flyway, but for Liquibase.
* /logfile — returns ordinary Java application logs.
* /loggers — enables us to query and modify the logging level of our application.
* /metrics — details the metrics of our application. This might include generic metrics as well as custom ones.
* /prometheus — returns metrics like the previous one, but formatted to work with a Prometheus server.
* /scheduledtasks — provides details about every scheduled task within our application.
* /sessions — lists HTTP sessions, given we are using Spring Session.
* /shutdown — performs a graceful shutdown of the application.
* /threaddump — dumps the thread information of the underlying JVM.

### Guides
The following guides illustrate how to use some features concretely:

* [Spring Boot Actuator documentation](https://docs.spring.io/spring-boot/docs/1.5.22.RELEASE/reference/html/production-ready-endpoints.html)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Building a RESTful Web Service with Spring Boot Actuator](https://spring.io/guides/gs/actuator-service/)

