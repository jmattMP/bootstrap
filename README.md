**This branch provide skeleton to create DDD application with some sample modules**

The most important module in DDD approach is **core**

- core (domain, application-core, core, business) with following MND (minimum needed dependency):
jUnit, assertJ, Mockito, Lombok, Logging. In this layer we do more then 80% of unit tests

- application (application, service, application service, rules, use cases) with following MND:
**domain**, jUnit, Mockito, assertJ, Lombok, MapStructure, Spring (Context, Core, Testing, maybe Transactions). In this layer we can perform
some component tests: 2-3 components, scenario tests, integration or e2e tests.

This two layers (can be more in case of complex domain) are out hear of application. Core model true business area rules
and application layer model application behaviour. Core should be as POJOs as possible (testing dependency, pure java)
and application should take into account context tests, convert from VO, DDD Entities, JPA Entities into presentation objects
like DTO, consider transaction, security, validation etc.

More over core + application layer cause full workable application but we need some external consumer/providers in order to fulfilled customer needs.
Domain expose interfaces for Repositories and Application expose interface for other communication like web-services, ui, e-mail service, messaging,
pdf or other infrastructure. We treat all of these interfaces as adapters and implementation as ports. Other microservices, services etc could be treated as infrastructure
business provides.

####Examples of ports modules:

#####UI/web-services

- api-gateway(proxy, security)
- web-rest-port(**application**, spring-web starter, jUnit, mockito, lombok, wiremock, restassured, asserJ, actuator, spring-boot-maven-plugin, swagger): REST-API with OpenDocu
- web-ui-port(**application**, spring-web starter, jUnit, mockito, lombok, wiremock, restassured, asserJ, actuator, spring-boot-maven-plugin, template engine like freemarker, mustache or thymeleaf):
  eg. web-ui-thymeleaf-port, web-ui-freemarker-port or just web-ui-freemarker
- others web-services(web-soap-port or web-graph-ql-port)
- ui for consuming web-services

#####storage

- storage-jpa-port(**application**, **domain**, jUnit/assertJ/Mockito, h2, lombok, hibernate, spring-jpa-data-starter)
- others depends on standard or software: storage-nosql-port, storage-cassandra-port, storage-couchbase-port, storage-kafka-port, storage-mysql-port, 

#####others ports

- smtp fake
- e-mail sender module
- pdf/excel readers
- logging managements
- in-memory storage like hazelcast
- event management

In order to testing application we can have two separate modules:

- integration-tests (test between two layers, e.g repository interface in domain + repository implementation) with following dependency: spring test context, jUnit/Mockito/assertJ
- acceptance-tests (jUnit/Mockito/AssertJ + cucumber/serenity)
