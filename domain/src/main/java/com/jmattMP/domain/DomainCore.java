package com.jmattMP.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DomainCore {

 private Logger logger = LoggerFactory.getLogger(DomainCore.class);

 public void loggerCore(){
     logger.trace("A TRACE Message");
     logger.debug("A DEBUG Message");
     logger.info("An INFO Message");
     logger.warn("A WARN Message");
     logger.error("An ERROR Message");
 }


}
