package com.jmattMP.webuithymeleafport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebUiThymeleafPortApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebUiThymeleafPortApplication.class, args);
	}

}
